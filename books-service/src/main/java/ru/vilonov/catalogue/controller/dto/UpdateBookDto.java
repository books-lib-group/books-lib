package ru.vilonov.catalogue.controller.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record UpdateBookDto(@NotNull
                            @Size(min = 3, max = 50, message = "{catalogue.books.create.errors.invalid_title}")
                            String title,
                            @Size(max = 1000, message = "{catalogue.books.create.errors.invalid_description}")
                            String description) {
}
