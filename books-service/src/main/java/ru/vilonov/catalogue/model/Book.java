package ru.vilonov.catalogue.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(schema = "public",name = "t_book")
public class Book {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "book_id", columnDefinition = "uuid",updatable = false)
    private UUID uuid;

    @NotNull
    @Column(name = "c_title", length = 50)
    private String title;

    @Column(name = "c_description", length = 1000)
    private String description;
}
