package ru.vilonov.catalogue.controller.dto;

import ru.vilonov.catalogue.model.Book;

import java.util.UUID;

public record BookDto(UUID uuid, String title, String description) {
    public static BookDto of(Book book){
        return new BookDto(book.getUuid(),book.getTitle(), book.getDescription());
    }
}
