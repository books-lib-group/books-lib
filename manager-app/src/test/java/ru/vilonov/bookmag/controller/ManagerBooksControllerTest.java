package ru.vilonov.bookmag.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import ru.vilonov.bookmag.client.BadRequestException;
import ru.vilonov.bookmag.controller.dto.NewBookDto;
import ru.vilonov.bookmag.model.Book;
import ru.vilonov.bookmag.services.ManagerService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ManagerBooksControllerTest {
    @Mock
    ManagerService managerService;
    @Mock
    BadRequestException exception;
    Model model;
    NewBookDto testBook;
    Map<String,Object> mapToModel;
    @InjectMocks
    ManagerBooksController managerBooksController;

    @BeforeEach
    void setUp() {
        this.model = new ConcurrentModel();
        this.testBook = new NewBookDto("title", "description");
        this.mapToModel = Map.of("book", testBook,"errors", List.of("error_1", "error_2"));
    }

    @Test
    void getBooksListPage() {
        // given
        String filter = "filter";
        List<Book> list = List.of(new Book(UUID.randomUUID(),"title", "description"));
        when(this.managerService.findAllBooks(filter)).thenReturn(list);
        // when
        String result = this.managerBooksController.getBooksListPage(model,filter);
        // then
        assertNotNull(result);
        assertEquals("catalogue/books/list", result);
        assertEquals(filter, model.getAttribute("filter"));
        assertEquals( list, model.getAttribute("books"));
    }

    @Test
    void createBook_RequestIsValid() {
        // given
        when(managerService.createBook(testBook.title(), testBook.description()))
                .thenReturn(Optional.empty());
        // when
        String result = managerBooksController.createBook(testBook, model);
        // then
        assertNotNull(result);
        assertEquals("redirect:/catalogue/books/list", result);
        verify(managerService).createBook(anyString(), anyString());
        assertTrue(model.asMap().isEmpty());
    }

    @Test
    void createBook_RequestNoValid() {
        // given
        ProblemDetail details = ProblemDetail.forStatus(HttpStatus.BAD_REQUEST);
        details.setProperties(mapToModel);
        when(exception.getProblemDetail()).thenReturn(details);
        when(managerService.createBook(testBook.title(), testBook.description()))
                .thenThrow(exception);
        // when
        String result = this.managerBooksController.createBook(testBook, model);
        // then
        assertNotNull(result);
        assertEquals("catalogue/books/create", result);
        verify(managerService).createBook(anyString(), anyString());
        Map<String,Object> mapFromModel = this.model.asMap();
        assertEquals(testBook, mapFromModel.get("book"));
        assertEquals(mapToModel.get("errors"), mapFromModel.get("errors"));
    }

}