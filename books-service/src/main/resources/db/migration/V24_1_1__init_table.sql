create table t_book(
    book_id uuid primary key not null,
    c_title varchar(50) not null check(length(trim(c_title)) >= 3),
    c_description varchar(1000)
);

