package ru.vilonov.catalogue.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;
import ru.vilonov.catalogue.controller.dto.BookDto;
import ru.vilonov.catalogue.controller.dto.UpdateBookDto;
import ru.vilonov.catalogue.model.Book;
import ru.vilonov.catalogue.service.BookService;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookRestControllerTest {

    @Mock
    BookService bookService;

    @InjectMocks
    BookRestController bookRestController;

    UUID uuid;

    @BeforeEach
    public void getUUID() {
        uuid = UUID.randomUUID();
    }

    @Test
    void findBook_returnBookDto() {
        // given
        Book book = new Book(uuid,"title","description");
        when(bookService.findBook(uuid)).thenReturn(book);
        // when
        var result = bookRestController.findBook(uuid.toString());
        // then
        assertNotNull(result);
        assertEquals(BookDto.of(book),result);
        verify(bookService).findBook(uuid);
    }

    @Test
    void updateBook_returnResponseEntityUpdateBookDto() throws BindException {
        // given
        Book book = new Book(uuid,"book","description");
        BindingResult bindingResult = mock(BindingResult.class);
        when(bookService.updateBook(uuid,"book", "description"))
                .thenReturn(book);
        // when
        var result = bookRestController.updateBook(
                uuid.toString(),new UpdateBookDto("book","description"),bindingResult);
        // then
        assertNotNull(result);
        assertEquals(HttpStatus.OK,result.getStatusCode());
        assertEquals(BookDto.of(book),result.getBody());
        verify(bookService).updateBook(uuid,"book", "description");
    }
    @Test
    void updateBook_throwBindingException() throws BindException {
        // given
        var bindingResult = new MapBindingResult(Map.of(), "newBookDto");
        bindingResult.addError(new FieldError("newBookDto", "title", "error"));
        // when
        var result = assertThrows(BindException.class, () -> bookRestController.updateBook(
                uuid.toString(),new UpdateBookDto("book","description"),bindingResult));
        // then
        assertNotNull(result);
        assertEquals(List.of(new FieldError("newBookDto", "title", "error")),
                result.getAllErrors());
        verifyNoInteractions(bookService);

    }

    @Test
    void deleteBook_returnResponseEntityVoid() {
        // given && when
        var result = bookRestController.deleteBook(uuid.toString());
        // then
        assertEquals(HttpStatus.NO_CONTENT,result.getStatusCode());
        verify(bookService).deleteBook(uuid);
    }
}