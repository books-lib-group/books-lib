package ru.vilonov.bookmag.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.vilonov.bookmag.client.BadRequestException;
import ru.vilonov.bookmag.model.Book;
import ru.vilonov.bookmag.controller.dto.UpdateBookDto;
import ru.vilonov.bookmag.aop.InjectUsername;
import ru.vilonov.bookmag.services.ManagerService;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
//Регулярное выражение проверяющее UUID: ^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$
@RequestMapping("/catalogue/books/{uuid:^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$}")
@RequiredArgsConstructor
public class ManagerBookController {
    private final ManagerService managerService;

    @ModelAttribute("book")
    public Book book(@PathVariable("uuid")String uuid) {
        return this.managerService.findBook(UUID.fromString(uuid)).orElseThrow();
    }

    @GetMapping
    private String getBookPage() {
        return "catalogue/books/book";
    }

    @GetMapping("edit")
    private String getBookEditPage() {
        return "catalogue/books/edit";
    }

    @PostMapping("edit")
    @InjectUsername
    public String editBook(@ModelAttribute("book") Book book, UpdateBookDto updateBookDto, Model model){
        try {
            this.managerService.updateBook(book.uuid(), updateBookDto.title(),updateBookDto.description());
            return "redirect:/catalogue/books/" + book.uuid();
        } catch (BadRequestException exception) {
            Map<String,Object> errors = exception.getProblemDetail().getProperties();
            model.addAttribute("book", new Book(book.uuid(), updateBookDto.title(), updateBookDto.description()));
            model.addAttribute("errors",(List<String>) errors.get("errors"));
            return "/catalogue/books/edit";
        }

    }

    @PostMapping("delete")
    public String deleteBook(@ModelAttribute("book") Book book){
        this.managerService.deleteBook(book.uuid());
        return "redirect:/catalogue/books/list";
    }


}

