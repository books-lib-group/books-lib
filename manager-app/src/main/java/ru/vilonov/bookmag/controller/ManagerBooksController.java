package ru.vilonov.bookmag.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.vilonov.bookmag.client.BadRequestException;
import ru.vilonov.bookmag.controller.dto.NewBookDto;
import ru.vilonov.bookmag.aop.InjectUsername;
import ru.vilonov.bookmag.services.ManagerService;


import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/catalogue/books/")
@RequiredArgsConstructor
public class ManagerBooksController {

    private final ManagerService managerService;

    @GetMapping("list")
    @InjectUsername
    public String getBooksListPage(Model model, @RequestParam(name = "filter", required = false) String filter){
        model.addAttribute("books", this.managerService.findAllBooks(filter));
        model.addAttribute("filter", filter);
        return "catalogue/books/list";
    }

    /**
     * Get запрос страницы добавления книги и ее возвращение
     * @return
     */
    @GetMapping("create")
    @InjectUsername
    public String getCreateBookPage(Model model){
        model.addAttribute("book",new NewBookDto("Title","Description"));
        return "catalogue/books/create";
    }

    /**
     * Post запроса с данными и добавление новой книги
     * @param newBookDto
     * @return перенаправление на список книг
     */
    @PostMapping("create")
    @InjectUsername
    public String createBook(NewBookDto newBookDto, Model model){
        try {
            this.managerService.createBook(newBookDto.title(), newBookDto.description());
            return "redirect:/catalogue/books/list";
        } catch (BadRequestException exception) {
            Map<String,Object> errors = exception.getProblemDetail().getProperties();
            model.addAttribute("book", newBookDto);
            model.addAttribute("errors",(List<String>) errors.get("errors"));
            return "catalogue/books/create";
        }

    }

}
