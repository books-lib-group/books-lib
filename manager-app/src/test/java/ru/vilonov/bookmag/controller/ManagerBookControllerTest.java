package ru.vilonov.bookmag.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import ru.vilonov.bookmag.client.BadRequestException;
import ru.vilonov.bookmag.controller.dto.UpdateBookDto;
import ru.vilonov.bookmag.model.Book;
import ru.vilonov.bookmag.services.ManagerService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ManagerBookControllerTest {
    @Mock
    ManagerService managerService;
    @Mock
    BadRequestException exception;
    @InjectMocks
    ManagerBookController managerBookController;
    Model model;
    Book testBook;
    Map<String,Object> mapToModel;

    @BeforeEach
    void setUp() {
        this.model = new ConcurrentModel();
        this.testBook = new Book(UUID.randomUUID(),"title", "description");
        this.mapToModel = Map.of("book", testBook,"errors", List.of("error_1", "error_2"));
    }

    @Test
    void book_bookFind() {
        // given
        when(managerService.findBook(Mockito.any())).thenReturn(Optional.of(testBook));
        // when
        Book book = managerBookController.book(UUID.randomUUID().toString());
        // then
        assertNotNull(book);
        assertEquals(testBook,book);
    }

    @Test
    void book_bookNotFind() {
        // given
        when(managerService.findBook(Mockito.any())).thenReturn(Optional.empty());
        // when && then
        assertThrows(NoSuchElementException.class, () -> {managerBookController.book(UUID.randomUUID().toString());});
    }

    @Test
    void editBook_RequestIsValid() {
        // given
        UpdateBookDto updateBookDto = new UpdateBookDto("title_update", "description_update");
        when(managerService.updateBook(testBook.uuid(),updateBookDto.title(),updateBookDto.description()))
                .thenReturn(Optional.of(new Book(testBook.uuid(),updateBookDto.title(),updateBookDto.description())));
        // when
        String result = managerBookController.editBook(testBook,updateBookDto,model);
        // then
        assertNotNull(result);
        assertEquals("redirect:/catalogue/books/" + testBook.uuid(), result);
        verify(managerService).updateBook(testBook.uuid(),updateBookDto.title(),updateBookDto.description());
    }

    @Test
    void editBook_RequestNotValid() {
        // given
        UpdateBookDto updateBookDto = new UpdateBookDto("title_update", "description_update");
        ProblemDetail details = ProblemDetail.forStatus(HttpStatus.BAD_REQUEST);
        details.setProperties(mapToModel);
        when(exception.getProblemDetail()).thenReturn(details);
        when(managerService.updateBook(testBook.uuid(),updateBookDto.title(), updateBookDto.description()))
                .thenThrow(exception);
        // when
        String result = managerBookController.editBook(testBook,updateBookDto,model);
        // then
        assertNotNull(result);
        assertEquals("/catalogue/books/edit", result);
        verify(managerService).updateBook(Mockito.any(UUID.class),Mockito.anyString(),Mockito.anyString());
        Map<String,Object> mapFromModel = this.model.asMap();
        assertEquals(new Book(testBook.uuid(), updateBookDto.title(), updateBookDto.description()), mapFromModel.get("book"));
        assertEquals(mapToModel.get("errors"), mapFromModel.get("errors"));
    }

    @Test
    void deleteBook() {
        // given
        String result = managerBookController.deleteBook(testBook);
        // when && then
        verify(managerService).deleteBook(testBook.uuid());
        assertEquals("redirect:/catalogue/books/list", result);
    }
}