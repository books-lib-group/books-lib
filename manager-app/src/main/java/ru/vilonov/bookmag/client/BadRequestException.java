package ru.vilonov.bookmag.client;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ProblemDetail;
import org.springframework.web.client.HttpClientErrorException;

@RequiredArgsConstructor
@Getter
public class BadRequestException extends RuntimeException{

    private final HttpClientErrorException exception;

    public ProblemDetail getProblemDetail(){
        return exception.getResponseBodyAs(ProblemDetail.class);
    }
}
