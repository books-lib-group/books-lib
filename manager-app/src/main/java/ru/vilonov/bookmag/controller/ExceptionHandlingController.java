package ru.vilonov.bookmag.controller;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.resource.NoResourceFoundException;
import ru.vilonov.bookmag.client.BadRequestException;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;

@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlingController {
    private final MessageSource messageSource;
    @ExceptionHandler({NoSuchElementException.class, NoResourceFoundException.class})
    private String handleNotFound(Exception exception, Model model,
            HttpServletResponse httpServletResponse, Locale locale) {
        httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
        model.addAttribute("error",
                this.messageSource.getMessage(exception.getMessage(), new Object[0], exception.getMessage(),locale));
        return "/errors/404";
    }
}
