package ru.vilonov.catalogue.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.vilonov.catalogue.controller.dto.BookDto;
import ru.vilonov.catalogue.controller.dto.NewBookDto;
import ru.vilonov.catalogue.model.Book;
import ru.vilonov.catalogue.service.BookService;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("catalogue-api/books")
public class BooksRestController {
    private final BookService bookService;

    @GetMapping
    @Operation(summary = "Get all books", security = @SecurityRequirement(name = "keycloak"))
    @ApiResponses({
            @ApiResponse(responseCode = "200",description = "Ok",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                               array = @ArraySchema(schema = @Schema(implementation = BookDto.class)))),
            @ApiResponse(responseCode = "401",description = "Unauthorized")})
    public ResponseEntity<List<BookDto>> getAllBooks(@RequestParam(name = "filter", required = false) String filter) {
        return ResponseEntity.ok()
                .body(this.bookService.findAllBooks(filter).stream().map(BookDto::of).toList());
    }

    @PostMapping
    @Operation(summary = "Create new book", security = @SecurityRequirement(name = "keycloak"))
    @ApiResponses({
            @ApiResponse(responseCode = "201",description = "Create",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = BookDto.class))),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = @Content),
            @ApiResponse(responseCode = "401",description = "Unauthorized",content = @Content)})
    public ResponseEntity<BookDto> createBook(
            @Valid @RequestBody NewBookDto newBookDto, BindingResult bindingResult,
            UriComponentsBuilder uriComponentsBuilder) throws BindException {
        if (bindingResult.hasErrors()) {
            if (bindingResult instanceof BindException exception) throw exception;
            else throw new BindException(bindingResult);
        }else {
            Book book = this.bookService.createNewBook(newBookDto.title(), newBookDto.description());
            return ResponseEntity.created(uriComponentsBuilder
                            .replacePath("catalogue-api/books/{uuid}")
                            .build(Map.of("uuid", book.getUuid())))
                    .body(BookDto.of(book));
        }
    }
}
