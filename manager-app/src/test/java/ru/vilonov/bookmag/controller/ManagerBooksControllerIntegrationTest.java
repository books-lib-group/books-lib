package ru.vilonov.bookmag.controller;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vilonov.bookmag.controller.dto.NewBookDto;
import ru.vilonov.bookmag.model.Book;

import java.util.List;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
@WireMockTest(httpPort = 9998)
class ManagerBooksControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void getBooksList_ReturnsBooksListPage() throws Exception {
        // given
        UUID firstUuid = UUID.randomUUID();
        UUID secondUuid = UUID.randomUUID();
        var requestBuilder = MockMvcRequestBuilders
                .get("/catalogue/books/list")
                .queryParam("filter", "book")
                .with(user("test").roles("MANAGER"));

        WireMock.stubFor(WireMock.get(WireMock.urlPathMatching("/catalogue-api/books"))
                .withQueryParam("filter", WireMock.equalTo("book"))
                .willReturn(WireMock.ok("""
                        [
                            {"uuid": "%s", "title": "book_1", "description": "description_1"},
                            {"uuid": "%s", "title": "book_2", "description": "description_2"}
                        ]""".formatted(firstUuid,secondUuid))
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(status().isOk(),
                        view().name("catalogue/books/list"),
                        model().attribute("filter", "book"),
                        model().attribute("books", List.of(
                                new Book(firstUuid, "book_1",  "description_1"),
                                new Book(secondUuid, "book_2",  "description_2")
                        ))
                );

        WireMock.verify(WireMock.getRequestedFor(WireMock.urlPathMatching("/catalogue-api/books"))
                .withQueryParam("filter", WireMock.equalTo("book")));
    }

    @Test
    void getBooksList_ReturnsForbidden() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/books/list")
                .queryParam("filter", "book")
                .with(user("test"));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isForbidden()
                );
    }

    @Test
    void getNewBookPage_ReturnsProductPage() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/books/create")
                .with(user("test").roles("MANAGER"));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        view().name("catalogue/books/create")
                );
    }

    @Test
    void getNewBookPage_ReturnsForbidden() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/products/create")
                .with(user("test"));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isForbidden()
                );
    }

    @Test
    void postBook_RedirectsToListPage() throws Exception {
        // given
        UUID uuid = UUID.randomUUID();
        var requestBuilder = MockMvcRequestBuilders
                .post("/catalogue/books/create")
                .param("title", "new_book")
                .param("description", "description_1")
                .with(user("test").roles("MANAGER"))
                .with(csrf());

        WireMock.stubFor(WireMock.post(WireMock.urlMatching("/catalogue-api/books"))
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": "new_book",
                            "description": "description_1"
                        }"""))
                .willReturn(WireMock.created()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withHeader(HttpHeaders.LOCATION, "/catalogue/books/" + uuid)
                        .withBody("""
                                {
                                    "uuid": "%s",
                                    "title": "new_book",
                                    "description": "description_1"
                                }""".formatted(uuid)))
        );

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(status().is3xxRedirection(),
                        view().name("redirect:/catalogue/books/list")
                );

        WireMock.verify(WireMock.postRequestedFor(WireMock.urlPathMatching("/catalogue-api/books"))
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": "new_book",
                            "description": "description_1"
                        }""")));
    }

    @Test
    void postProduct_ReturnsNewProductPageAndErrors() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders
                .post("/catalogue/books/create")
                .param("title", " ")
                .with(user("test").roles("MANAGER"))
                .with(csrf());

        WireMock.stubFor(WireMock.post(WireMock.urlPathMatching("/catalogue-api/books"))
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": " ",
                            "description": null
                        }"""))
                .willReturn(WireMock.badRequest()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PROBLEM_JSON_VALUE)
                        .withBody("""
                                {
                                    "errors": ["error_1", "error_2"]
                                }""")));


        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        view().name("catalogue/books/create"),
                        model().attribute("book", new NewBookDto(" ", null)),
                        model().attribute("errors", List.of("error_1", "error_2"))
                );

        WireMock.verify(WireMock.postRequestedFor(WireMock.urlPathMatching("/catalogue-api/books"))
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": " ",
                            "description": null
                        }""")));
    }

    @Test
    void createBook_ReturnsForbidden() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue/books/create")
                .param("title", "new_book")
                .param("description", "description_1")
                .with(user("test"))
                .with(csrf());

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isForbidden()
                );
    }

    @Test
    void createBook_returnFirstCallPage() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/books/create")
                .with(user("user").roles("MANAGER"))
                .with(csrf());

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        model().attribute("book", new NewBookDto("Title","Description")),
                        view().name("catalogue/books/create")
                );
    }
}