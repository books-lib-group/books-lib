package ru.vilonov.catalogue.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import ru.vilonov.catalogue.controller.dto.BookDto;
import ru.vilonov.catalogue.controller.dto.NewBookDto;
import ru.vilonov.catalogue.model.Book;
import ru.vilonov.catalogue.service.BookService;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BooksRestControllerTest {

    @Mock
    BookService bookService;
    @InjectMocks
    BooksRestController booksRestController;

    UUID uuid_1;
    UUID uuid_2;

    @BeforeEach
    void getUUID(){
        uuid_1 = UUID.randomUUID();
        uuid_2 = UUID.randomUUID();
    };

    @Test
    public void getAllBooks_returnResponseEntityListBooks() {
        // given
        String filter = "book";
        List<Book> listBooks = List.of(
                new Book(uuid_1,"book_1","description_1"),
                new Book(uuid_2,"book_2","description_2")
        );
        when(bookService.findAllBooks(filter)).thenReturn(listBooks);
        // when
        var result = booksRestController.getAllBooks(filter);
        // then
        assertNotNull(result);
        assertEquals(HttpStatus.OK,result.getStatusCode());
        assertEquals(listBooks.stream().map(BookDto::of).toList(),result.getBody());
    }

    @Test
    public void createBook_returnResponseEntityNewBook() throws BindException {
        // given
        BindingResult bindingResult = mock(BindingResult.class);
        Book book = new Book(uuid_1,"title","description");
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString("http://localhost");
        when(bookService.createNewBook("title","description")).thenReturn(book);
        when(bindingResult.hasErrors()).thenReturn(false);
        // when
        var result = booksRestController
                .createBook(new NewBookDto("title","description"),
                        bindingResult,uriComponentsBuilder);
        // then
        assertNotNull(result);
        assertEquals(HttpStatus.CREATED,result.getStatusCode());
        assertEquals(BookDto.of(book),result.getBody());
        assertEquals("http://localhost/catalogue-api/books/%s".formatted(uuid_1),
                result.getHeaders().get("Location").get(0));
        verify(bookService).createNewBook("title","description");
        verify(bindingResult).hasErrors();
    }

    @Test
    public void createBook_throwBindingException() throws BindException {
        // given
        var bindingResult = new MapBindingResult(Map.of(), "newBookDto");
        bindingResult.addError(new FieldError("newBookDto", "title", "error"));
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString("http://localhost");
        // when
        var result = assertThrows(BindException.class, () -> booksRestController.createBook(
                        new NewBookDto("title","description"),bindingResult,uriComponentsBuilder));
        // then
        assertNotNull(result);
        assertEquals(List.of(new FieldError("newBookDto", "title", "error")),
                result.getAllErrors());
        verifyNoInteractions(bookService);
    }

}