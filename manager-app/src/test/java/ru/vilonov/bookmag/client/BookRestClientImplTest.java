package ru.vilonov.bookmag.client;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.client.JdkClientHttpRequestFactory;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestClient;
import ru.vilonov.bookmag.config.ConfigTest;
import ru.vilonov.bookmag.model.Book;

import java.net.http.HttpClient;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


//@SpringBootTest
@Slf4j
@WireMockTest(httpPort = 9998)
@EnableConfigurationProperties(ConfigTest.class)
class BookRestClientImplTest {

    BookRestClientImpl bookRestClient = (new ConfigTest()).bookRestClientImpl("http://localhost:9998");;
    UUID uuid;

    @BeforeEach
    public void getUUID() {uuid = UUID.randomUUID();}

    @Test
    void createBook_returnedBookWithUuid() {
        // given
        WireMock.stubFor(WireMock.post(WireMock.urlMatching("/catalogue-api/books"))
                .willReturn(WireMock.okJson("""
                        {
                            "uuid": "%s",
                            "title": "new_book",
                            "description": "description_1"
                        }
                        """.formatted(uuid))));
        // when
        Book result = bookRestClient.createBook("new_book","description_1").get();
        // then
        assertNotNull(result);
        assertEquals(uuid, result.uuid());
    }

    @Test
    void createBook_InvalidRequest() {
        // given
        WireMock.stubFor(WireMock.post(WireMock.urlMatching("/catalogue-api/books"))
                .willReturn(WireMock.badRequest()));
        // when && then
        assertThrows(BadRequestException.class,() -> bookRestClient.createBook("  ",null).get());
    }

    @Test
    void updateBook_returnedUpdateBook() {
        // given
        WireMock.stubFor(WireMock.put(WireMock.urlMatching("/catalogue-api/books/" + uuid))
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": "new_book_1",
                            "description": "description_2"
                        }"""))
                .willReturn(WireMock.okJson("""
                        {
                            "uuid": "%s",
                            "title": "new_book_1",
                            "description": "description_2"
                        }
                        """.formatted(uuid))));
        // when
        Book result = bookRestClient.updateBook(uuid,"new_book_1","description_2").get();
        // then
        assertNotNull(result);
        assertEquals(uuid, result.uuid());
        assertEquals("new_book_1", result.title());
        assertEquals("description_2", result.description());
    }

    @Test
    void deleteBook_InvalidRequest() {
        // given
        WireMock.stubFor(WireMock.put(WireMock.urlMatching("/catalogue-api/books/" + uuid))
                .willReturn(WireMock.notFound()));
        // when && then
        assertThrows(NoSuchElementException.class,() -> bookRestClient.deleteBook(uuid));
    }

    @Test
    void findBook_returnedBook() {
        // given
        WireMock.stubFor(WireMock.get(WireMock.urlMatching("/catalogue-api/books/" + uuid))
                .willReturn(WireMock.okJson("""
                        {
                            "uuid": "%s",
                            "title": "new_book",
                            "description": "description_1"
                        }
                        """.formatted(uuid))));
        // when
        Book result = bookRestClient.findBook(uuid).get();
        // then
        assertNotNull(result);
        assertEquals(uuid, result.uuid());
        assertEquals("new_book", result.title());
        assertEquals("description_1", result.description());
    }

    @Test
    void findBook_InvalidRequest() {
        // given
        WireMock.stubFor(WireMock.get(WireMock.urlMatching("/catalogue-api/books/" + uuid))
                .willReturn(WireMock.notFound()));
        // when && then
        assertThrows(NoSuchElementException.class,() -> bookRestClient.findBook(uuid).get());
    }

    @Test
    void findAllBooks() {
        // given
        String filter = "test";
        WireMock.stubFor(WireMock.get(WireMock.urlMatching("/catalogue-api/books\\?filter=" + filter))
                .willReturn(WireMock.okJson("""
                        [
                            {
                                "uuid": "%s",
                                "title": "test_book",
                                "description": "description_1"
                            }
                        ]
                        """.formatted(uuid))));
        // when
        List<Book> result = bookRestClient.findAllBooks(filter);
        // then
        assertNotNull(result);
        Book resultBook = result.get(0);
        assertEquals(uuid, resultBook.uuid());
        assertEquals("test_book", resultBook.title());
        assertEquals("description_1", resultBook.description());
    }
}