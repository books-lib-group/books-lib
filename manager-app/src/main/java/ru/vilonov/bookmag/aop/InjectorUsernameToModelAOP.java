package ru.vilonov.bookmag.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.pulsar.PulsarProperties;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.security.Principal;

@Aspect
@Component
public class InjectorUsernameToModelAOP {
    @After("@annotation(InjectUsername) && args(model, ..)")
    public void injectUsernameToModel(Model model) {
        var user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String name = "anon";
        if (user instanceof UserDetails userDetails) {
            name = userDetails.getUsername();
        }
        if (user instanceof OAuth2AuthenticatedPrincipal principal) {
            name = principal.getAttribute("name");
        }
        model.addAttribute("username", name);
    }
}
