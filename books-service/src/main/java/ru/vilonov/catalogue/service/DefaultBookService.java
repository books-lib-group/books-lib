package ru.vilonov.catalogue.service;


import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.vilonov.catalogue.model.Book;
import ru.vilonov.catalogue.repository.BookRepository;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultBookService implements BookService {
    private final BookRepository bookRepository;

    @Override
    public List<Book> findAllBooks(String filter) {
        if (filter != null && !filter.isBlank() && !filter.equals("null")) {
            return Collections.unmodifiableList(this.bookRepository.findAllBooks(filter));
        }
        return Collections.unmodifiableList(this.bookRepository.findAll());
    }

    @Override
    @Transactional
    public Book createNewBook(String title, String description) {
        return this.bookRepository.save(Book.builder().title(title).description(description).build());
    }

    @Override
    @Transactional
    public Book findBook(UUID uuid) {
        return this.bookRepository.findById(uuid)
                .orElseThrow(() -> new NoSuchElementException("catalogue.errors.book.not_found"));
    }

    @Override
    @Transactional
    public Book updateBook(UUID uuid, String title, String description) {
        Book book = this.bookRepository
                .findById(uuid)
                .orElseThrow(() -> new NoSuchElementException("catalogue.errors.book.not_found"));
        book.setTitle(title);
        book.setDescription(description);
        return book;
    }

    @Override
    @Transactional
    public void deleteBook(UUID uuid) {
        this.bookRepository.deleteById(uuid);
    }


}
