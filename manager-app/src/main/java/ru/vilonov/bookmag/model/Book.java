package ru.vilonov.bookmag.model;

import java.util.UUID;

public record Book(UUID uuid, String title, String description) {
}
