package ru.vilonov.bookmag.controller.dto;

public record NewBookDto(String title, String description){
}
