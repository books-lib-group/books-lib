package ru.vilonov.bookmag.client;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;
import ru.vilonov.bookmag.controller.dto.NewBookDto;
import ru.vilonov.bookmag.controller.dto.UpdateBookDto;
import ru.vilonov.bookmag.model.Book;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
public class BookRestClientImpl implements BookRestClient{

    private final RestClient restClient;
    private static final ParameterizedTypeReference<List<Book>> LIST_BOOKS_REFERENCE =
            new ParameterizedTypeReference<>(){};

    @Override
    public Optional<Book> createBook(String title, String description) {
        try {
        return Optional.ofNullable(this.restClient.post()
                .uri("catalogue-api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .body(new NewBookDto(title,description))
                .retrieve()
                .body(Book.class));
        } catch (HttpClientErrorException.BadRequest exception) {
            throw new BadRequestException(exception);
        }
    }

    @Override
    public Optional<Book> updateBook(UUID uuid, String title, String description) {
        try {
            return Optional.ofNullable(this.restClient.put()
                    .uri("catalogue-api/books/" + uuid)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(new UpdateBookDto(title,description))
                    .retrieve()
                    .body(Book.class));
        } catch (HttpClientErrorException.BadRequest exception) {
            throw new BadRequestException(exception);
        }

    }

    @Override
    public void deleteBook(UUID uuid) {
        try {
            this.restClient.delete()
                    .uri("catalogue-api/books/" + uuid)
                    .retrieve()
                    .body(Book.class);
        } catch (HttpClientErrorException.NotFound exception) {
            throw new NoSuchElementException(exception);
        }

    }

    @Override
    public Optional<Book> findBook(UUID uuid) {
        try {
            return Optional.ofNullable(this.restClient.get()
                    .uri("/catalogue-api/books/" + uuid)
                    .retrieve()
                    .body(Book.class));
        } catch (HttpClientErrorException.NotFound exception) {
            throw new NoSuchElementException(exception);
        }
    }

    @Override
    public List<Book> findAllBooks(String filter) {
        return this.restClient.get()
                .uri("catalogue-api/books?filter={filter}",filter)
                .retrieve()
                .body(LIST_BOOKS_REFERENCE);
    }
}
