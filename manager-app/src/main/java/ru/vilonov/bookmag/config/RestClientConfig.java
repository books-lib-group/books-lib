package ru.vilonov.bookmag.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.JdkClientHttpRequestFactory;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.web.client.RestClient;
import ru.vilonov.bookmag.client.BookRestClientImpl;
import ru.vilonov.bookmag.security.Oauth2ClientHttpRequestInterceptor;

import java.net.http.HttpClient;

@Configuration
public class RestClientConfig {
    @Bean
    public BookRestClientImpl bookRestClient (
            @Value("${bookmag.services.catalogue.uri:http://localhost:8881}") String urlCatalogue,
            @Value("${bookmag.services.catalogue.registration-id::keycloak}") String registrationId,
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository
            ) {
        return new BookRestClientImpl(RestClient.builder()
                .baseUrl(urlCatalogue)
                .requestInterceptor(new Oauth2ClientHttpRequestInterceptor(
                        new DefaultOAuth2AuthorizedClientManager(
                                clientRegistrationRepository,
                                oAuth2AuthorizedClientRepository
                        ), registrationId))

                .build());
    }
}
