package ru.vilonov.bookmag.services;

import org.springframework.stereotype.Service;
import ru.vilonov.bookmag.model.Book;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public interface ManagerService {
    Optional<Book> createBook(String title, String description);
    Optional<Book> updateBook(UUID uuid, String title, String description);
    void deleteBook(UUID uuid);
    Optional<Book> findBook(UUID uuid);
    List<Book> findAllBooks(String filter);
}
