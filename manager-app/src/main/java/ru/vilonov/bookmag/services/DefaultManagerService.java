package ru.vilonov.bookmag.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vilonov.bookmag.client.BookRestClient;
import ru.vilonov.bookmag.model.Book;

import java.beans.Transient;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultManagerService implements ManagerService{
    @Autowired
    private final BookRestClient bookRestClient;

    public Optional<Book> createBook(String title, String description){
        return bookRestClient.createBook(title,description);
    }
    public Optional<Book> updateBook(UUID uuid, String title, String description){
        return bookRestClient.updateBook(uuid,title,description);
    }
    public void deleteBook(UUID uuid){
        bookRestClient.deleteBook(uuid);
    }
    public Optional<Book> findBook(UUID uuid){
        return bookRestClient.findBook(uuid);
    }
    public List<Book> findAllBooks(String filter){
        return bookRestClient.findAllBooks(filter);
    }
}
