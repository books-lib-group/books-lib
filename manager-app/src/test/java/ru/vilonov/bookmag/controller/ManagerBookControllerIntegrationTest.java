package ru.vilonov.bookmag.controller;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vilonov.bookmag.model.Book;

import java.util.List;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@WireMockTest(httpPort = 9998)
class ManagerBookControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;
    UUID uuid;

    @BeforeEach
    public void getUUID() {
        uuid = UUID.randomUUID();
    }

    @Test
    void getBook_ReturnsBookPage() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/books/" + uuid)
                .with(user("test").roles("MANAGER"));

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.okJson("""
                        {
                            "uuid": "%s",
                            "title": "new_book",
                            "description": "description_1"
                        }
                        """.formatted(uuid))));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        view().name("catalogue/books/book"),
                        model().attribute("book", new Book(uuid, "new_book", "description_1"))
                );
    }

    @Test
    void getBook_BookDoesNotExist() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/books/" + uuid)
                .with(user("test").roles("MANAGER"));

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.notFound()));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isNotFound(),
                        view().name("/errors/404")
                );
    }

    @Test
    void getBook_ReturnsForbidden() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/books/" + uuid)
                .with(user("test"));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isForbidden()
                );
    }

    @Test
    void getBookEditPage_ReturnsBookEditPage() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get(
                "/catalogue/books/%s/edit".formatted(uuid))
                .with(user("test").roles("MANAGER"));

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.okJson("""
                        {
                            "uuid": "%s",
                            "title": "new_book",
                            "description": "description_1"
                        }
                        """.formatted(uuid))));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        view().name("catalogue/books/edit"),
                        model().attribute("book", new Book(uuid, "new_book", "description_1"))
                );
    }

    @Test
    void getBookEditPage_BookDoesNotExist() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/books/%s/edit".formatted(uuid))
                .with(user("test").roles("MANAGER"));

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.notFound()));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isNotFound(),
                        view().name("/errors/404")
                );
    }

    @Test
    void getBookEditPage_ReturnsForbidden() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue/books/%s/edit".formatted(uuid))
                .with(user("test"));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isForbidden()
                );
    }

    @Test
    void updateBook_RedirectsToBooksListPage() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue/books/%s/edit".formatted(uuid))
                .param("title", "new_book_1")
                .param("description", "description_2")
                .with(user("test").roles("MANAGER"))
                .with(csrf());

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.okJson("""
                        {
                            "uuid": "%s",
                            "title": "new_book",
                            "description": "description_1"
                        }
                        """.formatted(uuid))));

        WireMock.stubFor(WireMock.put("/catalogue-api/books/" + uuid)
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": "new_book_1",
                            "description": "description_2"
                        }"""))
                .willReturn(WireMock.noContent()));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().is3xxRedirection(),
                        redirectedUrl("/catalogue/books/" + uuid)
                );

        WireMock.verify(WireMock.putRequestedFor(WireMock.urlPathMatching("/catalogue-api/books/" + uuid))
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": "new_book_1",
                            "description": "description_2"
                        }""")));
    }

    @Test
    void updateBook_ReturnsBookEditPage() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue/books/%s/edit".formatted(uuid))
                .param("title", " ")
                .with(user("test").roles("MANAGER"))
                .with(csrf());

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.okJson("""
                        {
                            "uuid": "%s",
                            "title": "new_book",
                            "description": "description_1"
                        }
                        """.formatted(uuid))));

        WireMock.stubFor(WireMock.put("/catalogue-api/books/" + uuid)
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": " ",
                            "description": null
                        }"""))
                .willReturn(WireMock.badRequest()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PROBLEM_JSON_VALUE)
                        .withBody("""
                                {
                                    "errors": ["error_1", "error_2"]
                                }""")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        view().name("/catalogue/books/edit"),
                        model().attribute("book", new Book(uuid, " ", null)),
                        model().attribute("errors", List.of("error_1", "error_2"))
                );

        WireMock.verify(WireMock.putRequestedFor(WireMock.urlPathMatching("/catalogue-api/books/" + uuid))
                .withRequestBody(WireMock.equalToJson("""
                        {
                            "title": " ",
                            "description": null
                        }""")));
    }

    @Test
    void updateBook_BookDoesNotExist() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue/books/%s/edit".formatted(uuid))
                .param("title", "new_book")
                .param("description", "description_1")
                .with(user("test").roles("MANAGER"))
                .with(csrf());

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.notFound()));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isNotFound(),
                        view().name("/errors/404")
                );
    }

    @Test
    void updateBook_ReturnsForbidden() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue/books/%s/edit".formatted(uuid))
                .param("title", "new_book")
                .param("description", "description_1")
                .with(user("test"))
                .with(csrf());

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isForbidden()
                );
    }

    @Test
    void deleteBook_RedirectsToBooksListPage() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue/books/%s/delete".formatted(uuid))
                .with(user("test").roles("MANAGER"))
                .with(csrf());

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.okJson("""
                        {
                            "uuid": "%s",
                            "title": "new_book",
                            "description": "description_1"
                        }
                        """.formatted(uuid))));

        WireMock.stubFor(WireMock.delete("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.noContent()));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().is3xxRedirection(),
                        redirectedUrl("/catalogue/books/list")
                );

        WireMock.verify(WireMock.deleteRequestedFor(WireMock.urlPathMatching("/catalogue-api/books/" + uuid)));
    }

    @Test
    void deleteBook_BookDoesNotExist() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue/books/%s/delete".formatted(uuid))
                .with(user("test").roles("MANAGER"))
                .with(csrf());

        WireMock.stubFor(WireMock.get("/catalogue-api/books/" + uuid)
                .willReturn(WireMock.notFound()));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isNotFound(),
                        view().name("/errors/404")
                );
    }

    @Test
    void deleteProduct_ReturnsForbidden() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue/products/%s/delete".formatted(uuid))
                .with(user("test"))
                .with(csrf());

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isForbidden()
                );
    }
}