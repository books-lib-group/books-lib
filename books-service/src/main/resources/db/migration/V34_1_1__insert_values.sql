DO $$
DECLARE
num int :=0;
BEGIN
	WHILE num < 10 LOOP
	INSERT INTO  t_book(book_id, c_title, c_description)
	VALUES(gen_random_uuid(), concat('BookFromFlyWay_',num),  substr(md5(random()::text), 0, 50));
	num := num+1;
	END LOOP;
END $$;