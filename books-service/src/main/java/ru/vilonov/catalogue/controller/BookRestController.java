package ru.vilonov.catalogue.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.vilonov.catalogue.controller.dto.BookDto;
import ru.vilonov.catalogue.controller.dto.UpdateBookDto;
import ru.vilonov.catalogue.model.Book;
import ru.vilonov.catalogue.service.BookService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Tag(name = "book-rest-controller")
//Регулярное выражение идентифицирующее UUID: ^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$
@RequestMapping("catalogue-api/books/{uuid:^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$}")
public class BookRestController {
    private final BookService bookService;

    @GetMapping
    @Operation(summary = "Find book by uuid", security = @SecurityRequirement(name = "keycloak"))
    @ApiResponses({
            @ApiResponse(responseCode = "200",description = "Ok",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = BookDto.class))),
            @ApiResponse(responseCode = "404",description = "Not Found",content = @Content),
            @ApiResponse(responseCode = "401",description = "Unauthorized",content = @Content)})
    public BookDto findBook(@PathVariable("uuid") String uuid) {
        return BookDto.of(this.bookService.findBook(UUID.fromString(uuid)));
    }

    @PutMapping
    @Operation(summary = "Update book by uuid", security = @SecurityRequirement(name = "keycloak"))
    @ApiResponses({
            @ApiResponse(responseCode = "200",description = "Ok",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = @Content),
            @ApiResponse(responseCode = "401",description = "Unauthorized",content = @Content),
            @ApiResponse(responseCode = "404",description = "Not Found",content = @Content)})
    public ResponseEntity<BookDto> updateBook(@PathVariable("uuid") String uuid,
                                        @Valid @RequestBody UpdateBookDto updateBookDto,
                                        BindingResult bindingResult
                                        ) throws BindException {
        if (bindingResult.hasErrors()) {
            if (bindingResult instanceof BindException exception) throw exception;
            else throw new BindException(bindingResult);
        } else {
            Book book = this.bookService
                    .updateBook(UUID.fromString(uuid), updateBookDto.title(), updateBookDto.description());
            return ResponseEntity
                    .ok()
                    .body(BookDto.of(book));
        }
    }

    @DeleteMapping
    @Operation(summary = "Delete book by uuid", security = @SecurityRequirement(name = "keycloak"))
    @ApiResponses({
            @ApiResponse(responseCode = "204",description = "No Content"),
            @ApiResponse(responseCode = "401",description = "Unauthorized",content = @Content)})
    public ResponseEntity<Void> deleteBook(@PathVariable("uuid") String uuid) {
        this.bookService.deleteBook(UUID.fromString(uuid));
        return ResponseEntity.noContent().build();
    }
}
