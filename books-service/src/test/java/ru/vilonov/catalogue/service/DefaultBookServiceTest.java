package ru.vilonov.catalogue.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.vilonov.catalogue.model.Book;
import ru.vilonov.catalogue.repository.BookRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DefaultBookServiceTest {

    @Mock
    BookRepository bookRepository;

    @InjectMocks
    DefaultBookService bookService;

    UUID uuid_1;
    UUID uuid_2;

    @BeforeEach
    void getUUID(){
        uuid_1 = UUID.randomUUID();
        uuid_2 = UUID.randomUUID();
    };

    @Test
    void findAllBooksUseFilter_ReturnUnmodifiableList() {
        // given
        String filter = "test";

        List<Book> listBooks = List.of(
                new Book(uuid_1,"book_1","description_1"),
                new Book(uuid_2,"book_2","description_2")
        );
        when(bookRepository.findAllBooks(filter)).thenReturn(listBooks);
        // when
        var result = bookService.findAllBooks(filter);
        // the
        assertNotNull(result);
        assertEquals(listBooks,result);
        assertThrows(UnsupportedOperationException.class,
                () -> result.add(new Book(UUID.randomUUID(),"book_3","description_3"))
        );
        verify(bookRepository).findAllBooks(filter);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"","null"})
    void findAllBooksNotUseFilter_ReturnUnmodifiableList(String filter) {
        // given
        List<Book> listBooks = List.of(
                new Book(uuid_1,"book_1","description_1"),
                new Book(uuid_2,"book_2","description_2")
        );
        when(bookRepository.findAll()).thenReturn(listBooks);
        // when
        var result = bookService.findAllBooks("");
        // then
        assertNotNull(result);
        assertEquals(listBooks,result);
        assertThrows(UnsupportedOperationException.class,
                () -> result.add(new Book(UUID.randomUUID(),"book_3","description_3"))
        );
        verify(bookRepository).findAll();
    }

    @Test
    void createNewBook_ReturnCreatedBook() {
        // given
        String title = "book";
        String description = "description";
        when(bookRepository.save(new Book(null,title,description)))
                .thenReturn(new Book(uuid_1,title,description));
        // when
        var result = bookService.createNewBook(title,description);
        // then
        assertNotNull(result);
        assertEquals(result,new Book(uuid_1,title,description));
        verify(bookRepository).save((new Book(null,title,description)));
    }

    @Test
    void findBook_returnBook() {
        // given
        String title = "book";
        String description = "description";
        Book book = new Book(uuid_1,title,description);
        when(bookRepository.findById(uuid_1)).thenReturn(Optional.of(book));
        // when
        var result = bookService.findBook(uuid_1);
        // then
        assertNotNull(result);
        assertEquals(uuid_1,result.getUuid());
        assertEquals(book,result);
        verify(bookRepository).findById(uuid_1);
    }

    @Test
    void findBook_BookNotFind() {
        // given
        when(bookRepository.findById(uuid_1)).thenReturn(Optional.empty());
        // when && then
        assertThrows(NoSuchElementException.class,() -> bookService.findBook(uuid_1));
        verify(bookRepository).findById(uuid_1);
    }

    @Test
    void updateBook_returnUpdatedBook() {
        // given
        String title = "book";
        String description = "description";
        Book book = new Book(uuid_1,title,description);
        when(bookRepository.findById(uuid_1)).thenReturn(Optional.of(book));
        // when
        var result = bookService.updateBook(uuid_1, "new_book","description_1");
        // then
        assertNotNull(result);
        assertEquals(uuid_1,result.getUuid());
        assertNotEquals(title,result.getTitle());
        assertNotEquals(description,result.getDescription());
        verify(bookRepository).findById(uuid_1);
    }

    @Test
    void deleteBook() {
        // given && when
        bookService.deleteBook(uuid_1);
        // then
        verify(bookRepository).deleteById(uuid_1);
        verifyNoMoreInteractions(bookRepository);
    }
}