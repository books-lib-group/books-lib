package ru.vilonov.catalogue.controller;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
class BookRestControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    UUID uuid;

    @BeforeEach
    public void getUUID() {
        uuid = UUID.fromString("123e4567-e89b-12d3-a456-556642440000");
    }

    @Test
    @Sql("/sql/test_books.sql")
    void findBook_ResponseEntityBookDto() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue-api/books/{uuid}",uuid)
                .with(jwt().jwt(builder -> builder.claim("scope", "view_catalogue")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                        content().json("""
                                {
                                    "uuid": "%s",
                                    "title": "book_1",
                                    "description": "description_1"
                                }""".formatted(uuid))
                );
    }

    @Test
    @Sql("/sql/test_books.sql")
    void findBook_ResponseEntityNotFound() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue-api/books/{uuid}",UUID.randomUUID())
                .with(jwt().jwt(builder -> builder.claim("scope", "view_catalogue")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(status().isNotFound());
    }

    @Test
    @Sql("/sql/test_books.sql")
    void updateBook_ResponseEntityUpdateBook() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.put("/catalogue-api/books/{uuid}",uuid)
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "title": "book_2",
                            "description": "description_2"
                        }
                        """)
                .with(jwt().jwt(builder -> builder.claim("scope", "edit_catalogue")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                        content().json("""
                                {
                                    "uuid": "%s",
                                    "title": "book_2",
                                    "description": "description_2"
                                }""".formatted(uuid))
                );
    }

    @Test
    @Sql("/sql/test_books.sql")
    void updateBook_ResponseEntityInvalidRequest() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.put("/catalogue-api/books/{uuid}",uuid)
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "title": " ",
                            "description": "description_2"
                        }
                        """)
                .with(jwt().jwt(builder -> builder.claim("scope", "edit_catalogue")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isBadRequest(),
                        content().contentTypeCompatibleWith(MediaType.APPLICATION_PROBLEM_JSON)
                );
    }

    @Test
    @Sql("/sql/test_books.sql")
    void deleteBook_ResponseEntityNoContent() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.delete("/catalogue-api/books/{uuid}",uuid)
                .with(jwt().jwt(builder -> builder.claim("scope", "edit_catalogue")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isNoContent()
                );
    }
}