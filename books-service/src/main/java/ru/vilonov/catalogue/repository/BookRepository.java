package ru.vilonov.catalogue.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.vilonov.catalogue.model.Book;

import java.util.List;
import java.util.UUID;

@Repository
public interface BookRepository extends JpaRepository<Book,UUID> {

    @Query(value = "SELECT * FROM t_book WHERE c_title ILIKE %:filter%", nativeQuery = true)
    public List<Book> findAllBooks(@Param("filter") String filter);
}
