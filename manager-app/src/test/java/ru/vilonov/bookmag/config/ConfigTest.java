package ru.vilonov.bookmag.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.JdkClientHttpRequestFactory;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.web.client.RestClient;
import ru.vilonov.bookmag.client.BookRestClientImpl;
import ru.vilonov.bookmag.services.DefaultManagerService;
import ru.vilonov.bookmag.services.ManagerService;

import java.net.http.HttpClient;

import static org.mockito.Mockito.mock;

@Configuration
@Slf4j
public class ConfigTest {
    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        return mock(ClientRegistrationRepository.class);
    }


    @Bean
    public OAuth2AuthorizedClientRepository oAuth2AuthorizedClientRepository() {
        return mock(OAuth2AuthorizedClientRepository.class);
    }
    @Bean
    @Primary
    public BookRestClientImpl bookRestClientImpl (
            @Value("${bookmag.services.catalogue.uri:http://localhost:9998}") String urlCatalogue
    ) {
        return new BookRestClientImpl(RestClient.builder()
                .baseUrl(urlCatalogue)
                .requestFactory(new JdkClientHttpRequestFactory(HttpClient.newBuilder()
                        .version(HttpClient.Version.HTTP_1_1)
                        .build()))
                .build());
    }

}
