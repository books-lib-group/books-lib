package ru.vilonov.bookmag.controller.dto;


public record UpdateBookDto(String title, String description) {
}
