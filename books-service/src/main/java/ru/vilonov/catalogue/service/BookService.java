package ru.vilonov.catalogue.service;

import org.springframework.stereotype.Service;
import ru.vilonov.catalogue.model.Book;

import java.util.List;
import java.util.UUID;

@Service
public interface BookService {
    List<Book> findAllBooks(String filter);
    Book createNewBook(String title, String description);
    Book findBook(UUID uuid);
    Book updateBook(UUID uuid, String title, String description);
    void deleteBook(UUID uuid);
}
