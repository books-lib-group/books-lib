package ru.vilonov.catalogue.controller;

import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Locale;


import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@Transactional
@SpringBootTest
@AutoConfigureMockMvc
class BooksRestControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    @Sql("/sql/test_books.sql")
    void getAllBooks_returnResponseEntityListBooks() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.get("/catalogue-api/books")
                .param("filter", "book_1")
                .with(jwt().jwt(builder -> builder.claim("scope", "view_catalogue")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isOk(),
                        content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                        content().json("""
                                [
                                    {
                                    "uuid": "123e4567-e89b-12d3-a456-556642440000", 
                                    "title": "book_1", 
                                    "description": "description_1"
                                    }
                                ]""")
                );
    }

    @Test
    void createBook_returnResponseEntityRequestInvalid() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue-api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {"title": "  ", "description": null}""")
                .with(jwt().jwt(builder -> builder.claim("scope", "edit_catalogue")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isBadRequest(),
                        content().contentTypeCompatibleWith(MediaType.APPLICATION_PROBLEM_JSON));
    }

    @Test
    void createBook_returnResponseEntityNewBook() throws Exception {
        // given
        var requestBuilder = MockMvcRequestBuilders.post("/catalogue-api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "title": "book_3", 
                            "description": "description_3"
                        }
                        """)
                .with(jwt().jwt(builder -> builder.claim("scope", "edit_catalogue")));

        // when
        this.mockMvc.perform(requestBuilder)
                // then
                .andDo(print())
                .andExpectAll(
                        status().isCreated(),
                        content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                );

    }
}